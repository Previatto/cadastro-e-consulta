from __future__ import unicode_literals
import psycopg2
import web
import time
#import csv
import xlwt
from StringIO import StringIO
# Try to connect

class dataBase:

    def connect(self):
        # colocar a base correta
        try:
            self.conn = psycopg2.connect("dbname='empiricus' user='gustavopreviatto' host='nvi-empiricus-dataserver-prd.ce8ilwv9vas6.us-east-1.rds.amazonaws.com' password='empiricus-2016'")
        except:
            print "I am unable to connect to the database."

        self.cur = self.conn.cursor()


    #insert = int(raw_input("Qual ID vc quer puxar?"))


    def create_base(self):

        try:
            # MUDR PARA INSERIR NO HISTORICO
            #MUDEI
            #self.cur.execute("""drop table analytics.fila_relacionamento""")
            self.cur.execute("""insert into analytics.relacionamento_historico
select row_number() over() as id, *
from ( select *
from (select distinct on (C."name") B.id_subs_details, B.duration, C.birth_date, E.qt_planos_ativos, C."name", C.email, C.document_number, C.phone, D.familia_planos, D.validade, 0 as status,date_trunc('day',now()) as data2, date_trunc('day',B.payment_date) as data1
from prognoos.subscription_details B
left join prognoos.customer C
on B.customer_id = C.customer_id
left join analytics.tb_dim_planos D
on B.plan_id = D.plan_id
left join analytics.clientes_ativos E
on C.email = E.email
where age(now(),B.payment_date) < '6 days' and age(now(),B.payment_date) > '5 days' and B.payment_confirmation_date is not null and C.phone <> ''
order by C."name") B
order by B.qt_planos_ativos, B.duration desc) C""",)

#             self.cur.execute("""create table analytics.fila_relacionamento as
# select row_number() over() as id, *
# from ( select *
# from (select distinct on (C."name") B.id_subs_details, B.duration, C.birth_date, E.qt_planos_ativos, C."name", C.email, C.document_number, C.phone, D.familia_planos, D.validade, 0 as status
# from prognoos.subscription_details B
# left join prognoos.customer C
# on B.customer_id = C.customer_id
# left join analytics.tb_dim_planos D
# on B.plan_id = D.plan_id
# left join analytics.clientes_ativos E
# on C.email = E.email
# where age(now(),B.payment_date) < '6 days' and age(now(),B.payment_date) > '5 days' and B.payment_confirmation_date is not null and C.phone <> ''
# order by C."name") B
# order by B.qt_planos_ativos, B.duration desc) C """, )
            self.conn.commit()
            #self.cur.execute("""INSERT INTO analytics.fila_relacionamento ("name", birth_date) VALUES (%s,%s)""",("Data","now()"))
            #self.conn.commit()
        except Exception as inst:
            print inst
            self.conn.rollback()
            print "I can't create database!"

        return True

    def fila(self,form):
        # criterios da base e de importancia
        #print plano
        rows = []
        try:
            # MUDEI PARA PEGAR DA FILA DO DIA
            if form.data1 != "":
                print form.data1 + "1"
                self.cur.execute(""" select * from analytics.relacionamento_historico where status < 2 and plano = %s and to_char(data1, 'DD/MM/YYYY') = %s order by status, id limit 1 """, (form.plano_lista,form.data1,))
            else:               
                print form.data1 
                self.cur.execute(""" select * from analytics.relacionamento_historico where status < 2 and plano = %s and data2 = date_trunc('day',now()) order by status, id limit 1 """, (form.plano_lista,))
            rows = self.cur.fetchall()
        except Exception as inst:
            self.conn.rollback()
            print inst
            print "I can't fila!"
        
        return rows



    def fim_fila(self,form):
        try:
            # MUDAR PARA PEGAR ID DA FILA DO DIA E DATA DO DIA
            #MUDEI
            self.cur.execute(""" update analytics.relacionamento_historico set tentativas = (select max(tentativas)+1 from analytics.relacionamento_historico where assinatura = %s), id = (select max(id)+1 from analytics.relacionamento_historico) where assinatura = %s and data2 = date_trunc('day',now())""", (form.assinatura,form.assinatura,))
            self.conn.commit()
        except Exception as inst:
            print "can't update"
            print inst
            self.conn.rollback()
        return True


    def check_base(self):
        rows = []
        try:
            # MUDAR PARA SELECT DO DIA E VER SE e VAZIO
            #self.cur.execute(""" select "name",birth_date from analytics.fila_relacionamento where "name"='Data' """)
            self.cur.execute(""" select * from analytics.relacionamento_historico where data2 = date_trunc('day',now()) """)
            rows = self.cur.fetchall()
        except:
            self.conn.rollback()
            print "I can't check!"

        
        if len(rows) ==0:
            return True
        else:
            
            return False

    def salva(self,form):
        try:
            # MUDAR PARA UPDATE
            #MUDEI
            #print form
            self.cur.execute("""UPDATE analytics.relacionamento_historico SET consultor=%s, efetivo=%s, humor=%s, realiza_invest=%s, corretora=%s
                , corretora2=%s, conhecimento=%s, area_logada=%s, cursos_monitoria=%s,cursos_motivo=%s, cancelamento_potencial=%s, obs=%s, status=2 
                tentativas = (select max(tentativas)+1 from analytics.relacionamento_historico where assinatura = %s)where assinatura=%s""",
            (form.consultor, form.efetivo, form.humor, form.invest, form.corret,form.corretora, form.conhecimento, form.logada, form.cursos, form.cursos2,
             form.cancelamento, form.statusinput,form.assinatura,form.assinatura,))
            self.conn.commit()
            # JUNTAR ALI EM CIMA
            #self.cur.execute("""update analytics.fila_relacionamento set status = 2 where id_subs_details = %s """, (form.assinatura,))
            #self.conn.commit()
        except Exception as inst:
            self.conn.rollback()
            print inst
            print "I can't insert into table"

    def salva2(self,form):
        try:
            # MUDAR PARA UPDATE
            #MUDEI
            #print form

            self.cur.execute("""INSERT INTO analytics.relacionamento_historico (id, assinatura ,duration ,nascimento ,qt_planos ,nome ,email ,cpf ,tel 
            ,plano ,tipo ,status ,data2 ,data1 ,tentativas ,consultor ,tipo_contato ,motivo_contato ,efetivo ,humor, realiza_invest , corretora , corretora2 ,
             conhecimento ,area_logada ,cursos_monitoria ,cursos_motivo,cancelamento_potencial ,obs) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
             date_trunc('day',now()),date_trunc('day',now()),%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) """,
            (-1,str(form.assinatura)," ",form.nascimento, " ", form.nome3, form.email, form.cpf, form.telo, form.plano, form.tipo, "2","1"  ,form.consultor,
                "receptivo" ,form.motivo_contato,form.efetivo, form.humor, form.invest, form.corret,form.corretora, form.conhecimento, form.logada, form.cursos, form.cursos2,
             form.cancelamento, form.statusinput,))
            self.conn.commit()
            # JUNTAR ALI EM CIMA
            #self.cur.execute("""update analytics.fila_relacionamento set status = 2 where id_subs_details = %s """, (form.assinatura,))
            #self.conn.commit()
        except Exception as inst:
            self.conn.rollback()
            print inst
            print "I can't update/insert into table"

    def extrai(self):
        try:
            #self.cur.execute("""select * from analytics.relacionamento_historico""")
            self.cur.execute("""select B.assinatura,C.status, C.date_canceled, B.status,B.data2,B.consultor,B.tentativas,B.nome, B.cpf, B.email, 
                B.tel, B.nascimento, B.plano,D.familia_planos ,B.tipo, B.tipo_contato, B.motivo_contato ,B.efetivo, 
                B.humor, B.realiza_invest, B.corretora, B.corretora2, B.conhecimento, B.area_logada,
                B.cursos_monitoria, B.cursos_motivo, B.cancelamento_potencial, B.obs
                from analytics.relacionamento_historico B
                left join prognoos.subscription_details C
                on B.assinatura = C.id_subs_details
                left join analytics.tb_dim_planos D
                on D.plan_id = C.plan_id""")

        except:
            self.conn.rollback()
        rows = self.cur.fetchall()    
        return rows
        # csvfile = StringIO()
        # spamwriter = csv.writer(csvfile)
        # spamwriter.writerow(['id','assinatura','duracao','nascimento','qt_planos_ativos', 'nome', 'email','cpf',  'tel', 'plano', 'tipo', 'status','data2','consultor', 'efetivo',
        #      'humor', 'realiza_invest', 'corretora', 'corretora2', 'conhecimento', 'area_logada', 'cursos_monitoria', 'cursos_motivo',
        #       'cancelamento_potencial', 'obs'])
        # for i in rows:
        #     spamwriter.writerow(i)
        # #with open('relatorio'+ time.strftime("%Y-%m-%d") +'.csv', 'wb') as csvfile:
        # # with open('relatorio.csv', 'wb') as csvfile:
        # #     spamwriter = csv.writer(csvfile)
        # #     # MUDAR OS NOMES CERTINHO
        # #     spamwriter.writerow(['id','assinatura','duracao','nascimento','qt_planos_ativos', 'nome', 'email','cpf',  'tel', 'plano', 'tipo', 'status','data2','consultor', 'efetivo',
        # #      'humor', 'realiza_invest', 'corretora', 'corretora2', 'conhecimento', 'area_logada', 'cursos_monitoria', 'cursos_motivo',
        # #       'cancelamento_potencial', 'obs'])
        # #     for i in rows:
        # #         spamwriter.writerow(i)
        # #getFile = file( './relatorio.csv', 'rb' )
        # # web.header('Content-type','application/octet-stream')
        # # web.header('Content-transfer-encoding','base64') 
        # web.header('Content-Type','text/csv')
        # web.header('Content-disposition', 'attachment; filename=relatorio.csv')
        # return csvfile.getvalue()
        
        #return csvfile


#cur.close()
#conn.close()
#conn.commit()




urls = (
  '/home2', 'Index',
  '/home3', 'Index3',
  '/relatorio','Info'
)

app = web.application(urls, globals())

render = web.template.render('templates/')

class Index(object):
    def GET(self):
        curo = dataBase()
        curo.connect()

        if curo.check_base():
            curo.create_base()

        form = web.input(plano_lista="",data1="")
        assinatura='Nenhum cliente'
        nome2=" "
        cpf=" "
        email=" "
        tel=" "
        nascimento=" "
        plano=" "
        tipo=" "
        chara = curo.fila(form)
        #print chara
        if len(chara) > 0:
            assinatura=chara[0][1]
            nome2=chara[0][5]
            cpf=chara[0][7]
            email=chara[0][6]
            tel=chara[0][8]
            nascimento=chara[0][3]
            plano=chara[0][9]
            tipo=chara[0][10]
        
        curo.cur.close()
        curo.conn.close()
        #nome2 = "teste" + form.ID
        #print  nome2
        return render.home2(assinatura=assinatura,nome2=nome2,cpf=cpf,email=email,tel=tel,nascimento=nascimento,plano=plano,tipo=tipo)
        #return render.home2()

    def POST(self):
        curo = dataBase()
        curo.connect()
        form = web.input()
        
        if form.efetivo == "sim":
            curo.salva(form)
        elif form.efetivo == "nao":
            curo.fim_fila(form)


        curo.cur.close()
        curo.conn.close()

        return render.home2(assinatura="",nome2="",cpf="",email="",tel="",nascimento="",plano="",tipo="")

class Index3(object):
    def GET(self):
        return render.home3()


    def POST(self):
        curo = dataBase()
        curo.connect()
        form = web.input(efetivo="",humor="", invest="", corret="",corretora="", conhecimento="", logada="",cursos="", cursos2="", cancelamento="")
        curo.salva2(form)
        curo.cur.close()
        curo.conn.close()

        return render.home3()


class Atua(object):
    def GET(self):
        return render.index(nome2='',tel='')

    def POST(self):
        return render.hello_f()


class Info(object):
    def GET(self):
        return True
    
    def POST(self):
        print "teste"

        curo = dataBase()
        curo.connect()
        form = web.input()

        if form.psw == "123456":
            a=curo.extrai()
            csvfile = StringIO()
            #spamwriter = csv.writer(csvfile)
            # spamwriter.writerow(['assinatura','status', 'atendida','data','consultor','tentativas','nome', 'cpf', 'email', 
            #     'tel', 'nascimento', 'plano', 'tipo', 'tipo_contato', 'motivo_contato' ,'efetivo', 
            #     'humor', 'realiza_invest', 'corretora', 'corretora2', 'conhecimento', 'area_logada',
            #     'cursos_monitoria', 'cursos_motivo', 'cancelamento_potencial', 'obs'])
                # ['id','assinatura','duracao','nascimento','qt_planos_ativos', 'nome', 'email','cpf',  'tel', 'plano', 'tipo', 'status','data2','consultor', 'efetivo',
                #  'humor', 'realiza_invest', 'corretora', 'corretora2', 'conhecimento', 'area_logada', 'cursos_monitoria', 'cursos_motivo',
                #   'cancelamento_potencial', 'obs'])
            # for i in a:
            #     spamwriter.writerow(i)
            #web.header('Content-Type','text/csv')
            #mimetype = 'application/vnd.ms-excel'
            #subject = u'Statistics Report'
            wb = xlwt.Workbook('utf-8')
            ws = wb.add_sheet('A Test Sheet')
            
            colunas = ['assinatura','status', 'data_cancelado','atendida','data','consultor','tentativas','nome', 'cpf', 'email', 
                'tel', 'nascimento', 'plano', 'plano_atual','tipo', 'tipo_contato', 'motivo_contato' ,'efetivo', 
                'humor', 'realiza_invest', 'corretora', 'corretora2', 'conhecimento', 'area_logada',
                'cursos_monitoria', 'cursos_motivo', 'cancelamento_potencial', 'obs']
            for i in range(len(colunas)):
                ws.write(0,i,colunas[i])
            for l in range(len(a)):
                for c in range(len(a[l])):
                    if type(a[l][c]) == int:
                        try:
                            ws.write(1+l,c,str(a[l][c]))
                        except:
                            ws.write(1+l,c,'error')
                    elif type(a[l][c]) == int:
                        try:
                            ws.write(1+l,c,str(a[l][c]))
                        except:
                            ws.write(1+l,c,'error')
                    else:
                        try:
                            ws.write(1+l,c,a[l][c].encode())
                        except:
                            try:
                                ws.write(1+l,c,str(a[l][c]))
                            except:
                                print type(a[l][c])
                                ws.write(1+l,c,'error')   

            wb.save(csvfile)
            web.header('Content-Type','application/vnd.ms-excel')
            web.header('Content-disposition', 'attachment; filename=export.xls')
            
            return csvfile.getvalue()
        else:
            #return False
            return render.home2(assinatura="",nome2="",cpf="",email="",tel="",nascimento="",plano="",tipo="")

        #return render.home2(assinatura="",nome2="",cpf="",email="",tel="",nascimento="",plano="",tipo="")



if __name__ == "__main__":
    #curo=dataBase()
    #curo.connect()
    app.run()